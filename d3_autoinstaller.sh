#!/bin/bash

# MAKE SURE WE ARE RUNNING AS ROOT

if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 
   exit 1
fi

# CHECK FOR PRE-REQUISITS FIRST

IPSET_VERSION="$(which ipset)"
if [ -z "$IPSET_VERSION" ]
then
      echo "ERROR: IPSET IS NOT INSTALLED"
      echo "Please install IPSET first using the command <sudo apt install ipset>"
      echo "Do you want me to try to do this for you? (y/n) "
      read install_ipset
      if [ "$install_ipset" = "y" ]
      then
          sudo apt install ipset
      else
          exit 1
      fi
else
      echo "SUCCESS: IPSET IS INSTALLED"
fi

RSYSLOG_CHECK="$(service rsyslog status | grep 'Active:')"
if [ -z "$RSYSLOG_CHECK" ]
then
      echo "ERROR: RSYSLOG IS NOT ACTIVE"
      echo "Please install rsyslog and make sure it is running before continuing"
      exit 1
else
      echo "SUCCESS: RSYSLOG IS ACTIVE"
      echo "Attempting to configure RSyslog For You...."
      echo "What is the address for your Dark Cubed syslog sensor? "
      read syslog_sensor
      printf "# SEE BELOW FOR DARK CUBED DATA\n*.*   @$syslog_sensor" >> /etc/rsyslog.conf
      service rsyslog restart
fi

#exit 1
echo "What is your Dark Cubed blocklist URL? "
read blocklist_url

# NOW WE WRITE OUR BLOCK LIST SCRIPT

echo "Creating the directory /usr/share/d3-autoblock and putting our blocklist script there"

mkdir -p /usr/share/d3-autoblock

echo "#!/bin/bash
/usr/bin/curl -s $blocklist_url > /usr/share/d3-autoblock/blocklist.txt
/sbin/ipset create d3blocklist hash:ip hashsize 4096 &> /dev/null
/sbin/ipset flush d3blocklist
while read ip; do
  /sbin/ipset add d3blocklist \$ip &> /dev/null
done </usr/share/d3-autoblock/blocklist.txt
rm /usr/share/d3-autoblock/blocklist.txt" > /usr/share/d3-autoblock/darkcubed_block_script.sh

chmod 755 /usr/share/d3-autoblock/darkcubed_block_script.sh

echo "Installing Crontab entry to run every 10 minutes"
(crontab -l ; echo "*/10 * * * * /usr/share/d3-autoblock/darkcubed_block_script.sh") | crontab

echo "Running script to sync blocklist now!"
/usr/share/d3-autoblock/darkcubed_block_script.sh

echo "Creating IPTables Rule for Blocklist"
iptables -I INPUT -m set --match-set d3blocklist src -j DROP
iptables -I INPUT -m set --match-set d3blocklist dst -j DROP

echo "Complete!!"
